import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Drink{
    String name;
    int cnt;
    int price;
    int totalCnt;
}

public class DrinkGUI {
    private JPanel root;
    private JTextPane ordersList;
    private JButton checkOut;
    private JButton cocaColaButton;
    private JButton ciderButton;
    private JButton ccLemonButton;
    private JButton gogoTeaButton;
    private JButton deathSauceButton;
    private JLabel topLabel;
    private JButton colaM;
    private JButton colaP;
    private JLabel colaC;
    private JLabel totalPrice;
    private JButton Addition;
    private JButton ciderM;
    private JButton ciderP;
    private JLabel ciderC;
    private JButton lemonM;
    private JButton lemonP;
    private JLabel lemonC;
    private JButton gogoM;
    private JButton gogoP;
    private JLabel gogoC;
    private JButton oranginaButton;
    private JButton oranginaM;
    private JButton oranginaP;
    private JLabel oranginaC;
    private JButton deathM;
    private JButton deathP;
    private JLabel deathC;
    private JPanel drink0;
    private JPanel drink1;
    private JPanel drink2;
    private JPanel drink3;
    private JPanel drink4;
    private JPanel drink5;
    private JPanel ordersListPanel;
    private JLabel colaL;
    private JLabel ciderL;
    private JLabel lemonL;
    private JLabel gogoL;
    private JLabel oranginaL;
    private JLabel deathL;
    private int total = 0;

    Drink[] drink = new Drink[6];

    public DrinkGUI() {
        cocaColaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Cola.jpg")
        ));
        ciderButton.setIcon(new ImageIcon(
                this.getClass().getResource("Cider.jpg")
        ));
        ccLemonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Lemon.jpg")
        ));
        gogoTeaButton.setIcon(new ImageIcon(
                this.getClass().getResource("gogotea.png")
        ));
        oranginaButton.setIcon(new ImageIcon(
                this.getClass().getResource("orangina.png")
        ));
        deathSauceButton.setIcon(new ImageIcon(
                this.getClass().getResource("Will.jpg")
        ));

        for(int i=0; i<drink.length; i++){
            drink[i] = new Drink();
        }

        drink[0].name = "COCA COLA";
        drink[0].cnt = 0;
        drink[0].price = 150;
        drink[0].totalCnt = 0;

        drink[1].name = "MITSUYA CIDER";
        drink[1].cnt = 0;
        drink[1].price = 170;
        drink[1].totalCnt = 0;

        drink[2].name = "CC LEMON";
        drink[2].cnt = 0;
        drink[2].price = 180;
        drink[2].totalCnt = 0;

        drink[3].name = "GOGO TEA";
        drink[3].cnt = 0;
        drink[3].price = 160;
        drink[3].totalCnt = 0;

        drink[4].name = "ORANGINA";
        drink[4].cnt = 0;
        drink[4].price = 210;
        drink[4].totalCnt = 0;

        drink[5].name = "WILKINSON";
        drink[5].cnt = 0;
        drink[5].price = 32600;
        drink[5].totalCnt = 0;

        String defaultText = ordersList.getText();

        colaM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(drink[0].totalCnt >= 1 || drink[0].cnt >= 1) {
                    drink[0].cnt--;
                    colaC.setText("" + drink[0].cnt);
                }
            }
        });
        colaP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drink[0].cnt++;
                colaC.setText("" + drink[0].cnt);
            }
        });
        ciderM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(drink[1].totalCnt >= 1 || drink[1].cnt >= 1) {
                    drink[1].cnt--;
                    ciderC.setText("" + drink[1].cnt);
                }
            }
        });
        ciderP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drink[1].cnt++;
                ciderC.setText("" + drink[1].cnt);
            }
        });
        lemonM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(drink[2].totalCnt >= 1 || drink[2].cnt >= 1) {
                    drink[2].cnt--;
                    lemonC.setText("" + drink[2].cnt);
                }
            }
        });
        lemonP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drink[2].cnt++;
                lemonC.setText("" + drink[2].cnt);
            }
        });
        gogoM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(drink[3].totalCnt >= 1 || drink[3].cnt >= 1) {
                    drink[3].cnt--;
                    gogoC.setText("" + drink[3].cnt);
                }
            }
        });
        gogoP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drink[3].cnt++;
                gogoC.setText("" + drink[3].cnt);
            }
        });
        oranginaM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(drink[4].totalCnt >= 1 || drink[4].cnt >= 1) {
                    drink[4].cnt--;
                    oranginaC.setText("" + drink[4].cnt);
                }
            }
        });
        oranginaP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drink[4].cnt++;
                oranginaC.setText("" + drink[4].cnt);
            }
        });
        deathM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(drink[5].totalCnt >= 1 || drink[5].cnt >= 1) {
                    drink[5].cnt--;
                    deathC.setText("" + drink[5].cnt);
                }
            }
        });
        deathP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drink[5].cnt++;
                deathC.setText("" + drink[5].cnt);
            }
        });

        Addition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean fin = false;
                String order = "";
                for(int i=0; i<drink.length; i++){
                    if(drink[i].cnt != 0){
                        order += drink[i].name + ": " + drink[i].cnt + " * " + drink[i].price + " Yen\n";
                        if(drink[i].totalCnt + drink[i].cnt < 0){
                            JOptionPane.showMessageDialog(null, "The number's of order must be 0 or greater.");
                            fin = true;
                            break;
                        }
                    }
                }

                if(order == "") {
                    JOptionPane.showMessageDialog(null, "You should order.");
                    fin = true;
                }
                if(!fin) {
                    int comfirmation = JOptionPane.showConfirmDialog(
                            null,
                            "Would you like to order drink?\n" +
                                    "------------------------------\n" +
                                    order,
                            "Order confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if (comfirmation == 0) {
                        String currentText = ordersList.getText();
                        ordersList.setText(currentText + order);
                        for (int i = 0; i < drink.length; i++) {
                            total += drink[i].cnt * drink[i].price;
                        }

                        totalPrice.setText(total + " Yen");
                        JOptionPane.showMessageDialog(null, "Thank you for ordering! It will take soon.");

                        for (int i = 0; i < drink.length; i++) {
                            drink[i].totalCnt += drink[i].cnt;
                            drink[i].cnt = 0;
                        }
                        colaC.setText("0");
                        ciderC.setText("0");
                        lemonC.setText("0");
                        gogoC.setText("0");
                        oranginaC.setText("0");
                        deathC.setText("0");
                    }
                }
            }
        });
        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int comfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(comfirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + " Yen.");
                    total = 0;
                    for(int i=0; i<drink.length; i++){
                        drink[i].totalCnt = 0;
                        drink[i].cnt = 0;
                    }
                    totalPrice.setText(total + "Yen");
                    ordersList.setText(defaultText);
                    colaC.setText("0");
                    ciderC.setText("0");
                    lemonC.setText("0");
                    gogoC.setText("0");
                    oranginaC.setText("0");
                    deathC.setText("0");
                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("DrinkGUI");
        frame.setContentPane(new DrinkGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
